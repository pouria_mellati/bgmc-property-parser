organization := "BlueGrassMC"

name := "BGMC-Property-Parser"

moduleName := "BGMC-Property-Parser"

version := "0.1.1-SNAPSHOT"

scalaVersion := "2.10.2"

// Prevent the eclipse plugin from creating src/main/java and src/main/scala.

unmanagedSourceDirectories in Compile <<= (scalaSource in Compile)(Seq(_))

unmanagedSourceDirectories in Test <<= (scalaSource in Test)(Seq(_))

libraryDependencies += "BlueGrassMC" %% "BGMC-Core" % "0.1.3-SNAPSHOT"
