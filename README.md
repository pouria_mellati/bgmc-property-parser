Parses a property file into an AST consumable by [BGMC core](https://bitbucket.org/pouria_mellati/bluegrassmodelchecker).
