package bgmc

import bgmc.property.parser.PropertyParser
import java.io.FileReader
import bgmc.property.parser.treeprocessing.ParserNodeTreeToValidPropertyFormula

// Test an input file here.
object Main extends PropertyParser {
  def main(args: Array[String]): Unit = {
    val fileReader = new FileReader(args(0))
    val parsed = parseAll(property, fileReader)
    println(s"Parsed:\n$parsed")
    val form = ParserNodeTreeToValidPropertyFormula.translate(parsed.get.get)
    println(form)
  }
}