package bgmc.property.parser.treeprocessing

import bgmc.property.parser._
import bgmc.{property => o}	// 'o' as in 'the Output format'.
import scala.collection.immutable.Set

private[treeprocessing] object CanonicalParserNodeTreeToPropertyFormulaTranslator {
  def xlate(f: Node, vars: Set[String]/* = Set.empty[String]*/): o.Formula = f match {
      case UnOp("N", sub) => o.Next(xlate(sub, vars))
      case BinOp("&&", l, r) => o.And(xlate(l, vars), xlate(r, vars))
      case BinOp("||", l, r) => o.Or(xlate(l, vars), xlate(r, vars))
      case BinOp("U", l, r) => o.Until(xlate(l, vars), xlate(r, vars))
      case BinOp("R", l, r) => o.Release(xlate(l, vars), xlate(r, vars))
      case QuantOp("ALL", v, r, f) => o.UniversalQuantifier(o.VariableId(v), xlateDotted(r, vars), xlate(f, vars + v))
      case QuantOp("EXS", v, r, f) => o.ExistentialQuantifier(o.VariableId(v), xlateDotted(r, vars), xlate(f, vars + v))
      case Bool(bExpr) => o.Literal(xlateBoolable(bExpr, vars))
      case unexpected => throw new PropertyParsingError(s"Encountered an unexpected operation when processing the canonicalized parse tree:\n$unexpected")
    }
  
  def xlateDotted(dotted: Dotted, quantificationVars: Set[String]): o.DottedExpr = {
    val dottedHead = dotted.chain.head
    val dottedTail = dotted.chain.tail
    if(quantificationVars contains dottedHead)
      o.DottedExpr(o.VariableId(dottedHead), dottedTail)
    else
      o.DottedExpr(o.ClassId(dottedHead), dottedTail) 
  }
  
  def xlateBoolable(b: Boolable, vars: Set[String]): o.BoolExpression = b match {
      case True => o.True
      case False => o.False
      case d: Dotted => o.AsBoolExpr(xlateDotted(d, vars))
      case Neg(expr) => o.Neg(xlateBoolable(expr, vars))
      case ComparisonOp(op, l, r) => o.Comparison(xlateComparisonOp(op), xlateDotted(l, vars), xlateDotted(r, vars))
    }
  
  def xlateComparisonOp(op: String): o.ComparisonOperator = op match {
      case "==" => o.Equality
      case "!=" => o.Unequality
      case ">"  => o.GreaterThan
      case "<"  => o.LessThan
      case unexpected => throw new PropertyParsingError(s"Encountered an unexpected comparison operator: $unexpected")
    }
}