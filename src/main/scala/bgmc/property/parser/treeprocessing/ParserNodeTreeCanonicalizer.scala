package bgmc.property.parser.treeprocessing

import bgmc.property.parser._

// Removes Globallies, Finallies, Weak Untils and early negations from a formula by rewriting them to their equivalents.
private[treeprocessing] object ParserNodeTreeCanonicalizer {
  def canonicalize(f: Node): Node =
    remEarlyNegs(remThens(remFs(remGs(remWs(f)))))
  
  private def remWs(f: Node): Node = f match {
      case BinOp("W", l, r) => BinOp("||", BinOp("U", remWs(l), remWs(r)), UnOp("G", remWs(l)))
      case other => applyFuncOnAllSubFormulasOf(other, remWs)
    }
  
  private def remGs(f: Node): Node = f match {
      case UnOp("G", sub) => UnOp("!", UnOp("F", UnOp("!", remGs(sub))))
      case other => applyFuncOnAllSubFormulasOf(other, remGs)
    }
  
  private def remFs(f: Node): Node = f match {
      case UnOp("F", sub) => BinOp("U", Bool(True), remFs(sub))
      case other => applyFuncOnAllSubFormulasOf(other, remFs)
    }
  
  private def remThens(f: Node): Node = f match {
      case BinOp("->", l, r) => BinOp("||", UnOp("!", remThens(l)), remThens(r))
      case other => applyFuncOnAllSubFormulasOf(other, remThens)
    }
  
  // Enforces NNF. Expects its parameter to contain no W's, G's, F's or ->'s.
  private def remEarlyNegs(f: Node): Node = f match {
      case UnOp("!", negated) => negated match {
          case Bool(expr) => Bool(Neg(expr))	// TODO: Will Neg's pile-up in expressions?
          case UnOp("!", sub) => remEarlyNegs(sub)
          case UnOp("N", sub) => UnOp("N", remEarlyNegs(UnOp("!", sub)))
          case BinOp("&&", l, r) => BinOp("||", remEarlyNegs(UnOp("!", l)), remEarlyNegs(UnOp("!", r)))
          case BinOp("||", l, r) => BinOp("&&", remEarlyNegs(UnOp("!", l)), remEarlyNegs(UnOp("!", r)))
          case BinOp("U" , l, r) => BinOp("R" , remEarlyNegs(UnOp("!", l)), remEarlyNegs(UnOp("!", r)))
          case QuantOp("ALL", v, r, sub) => QuantOp("EXS", v, r, remEarlyNegs(UnOp("!", sub)))
          case QuantOp("EXS", v, r, sub) => QuantOp("ALL", v, r, remEarlyNegs(UnOp("!", sub)))
          case unexpected =>
            throw new PropertyParsingError(s"""Uncountered an unexpected formula in the immediate context of a "!":\n$unexpected""")
        }
      case other => applyFuncOnAllSubFormulasOf(other, remEarlyNegs)
    }
  
  private def applyFuncOnAllSubFormulasOf(formula: Node, func: Node => Node): Node = formula match {
      case BinOp(op, l, r)  => BinOp(op, func(l), func(r))
      case UnOp(op, sub) => UnOp(op, func(sub))
      case QuantOp(op, v, r, sub) => QuantOp(op, v, r, func(sub))
      case b: Bool => b
    }
}