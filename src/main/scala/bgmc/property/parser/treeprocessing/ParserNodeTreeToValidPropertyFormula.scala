package bgmc.property.parser.treeprocessing

import bgmc.property.parser.Node
import bgmc.property.Formula

object ParserNodeTreeToValidPropertyFormula {
  def translate(parsedProperty: Node): Formula = {
    val canonicalized = ParserNodeTreeCanonicalizer.canonicalize(parsedProperty)
    CanonicalParserNodeTreeToPropertyFormulaTranslator.xlate(canonicalized, Set.empty[String])
  } 
}