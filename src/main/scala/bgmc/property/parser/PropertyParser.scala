package bgmc.property.parser

import scala.util.parsing.combinator._
import bgmc.property.Formula
import bgmc.property.parser.treeprocessing.ParserNodeTreeToValidPropertyFormula

class PropertyParser extends JavaTokenParsers {
  def parsePropertyAndNegate(propertyFile: java.io.FileReader): Formula =
    parseAll(property, propertyFile) match {
      case Success(nodeTree, _) =>
        ParserNodeTreeToValidPropertyFormula.translate(UnOp("!", nodeTree.get))
      case failure: NoSuccess =>
        throw new PropertyParsingError(s"Couldn't parse the property.\n${failure.msg}") 
    }
  
  def property: Parser[Option[Node]] = "property" ~> "{" ~> (block*) <~ "}" ^^ {filterOutTheGarbage(_).headOption}
  
  def block: Parser[NodeOrGarbage] =
    comment |
    unsupportedBlock("define") |
    unsupportedBlock("LTL") |
    unsupportedBlock("CTL") |
    unsupportedBlock("TCTL") |
    ltlQSection
    
  def comment: Parser[NodeOrGarbage] = ("""//.*\n""".r	| """/\*(?s).*\*/""".r) ^^ {x => Garbage}
  
  def ltlQSection: Parser[Node] = "LTL-Q" ~> "{" ~> ((ltlQProperty | comment)*) <~ "}" ^^ filterOutTheGarbage ^^ {_ reduce {BinOp("&&", _, _)}}
  
  def ltlQProperty: Parser[Node] = ident ~> ":" ~> ltlQExpr <~ ";"
  
  def ltlQExpr: Parser[Node] = or |  "!" ~> ltlQExpr ^^ {UnOp("!", _)}
  def or: Parser[Node] = and ~ rep(("OR" | "||") ~> and) ^^ {
    case node ~ Nil => node
    case frstOperand ~ rest => (List(frstOperand) ++ rest) reduce {BinOp("||", _, _)}
  }
  
  def and: Parser[Node] =  then ~ rep(("AND" | "&&") ~> then) ^^ {
    case node ~ Nil => node
    case frstOperand ~ rest => (List(frstOperand) ++ rest) reduce {BinOp("&&", _, _)}
  }
  
  def then: Parser[Node] = temporal ~ (("->" ~> temporal)*) ^^ {
    case node ~ Nil => node
    case frstOperand ~ rest => (List(frstOperand) ++ rest) reduce {BinOp("->", _, _)}
  }
  
  def temporal: Parser[Node] =
    ("G" | "F" | "N") ~ "(" ~ ltlQExpr ~ ")" ^^ {case op~"("~ltl~")" => UnOp(op, ltl)} |
    ("U" | "W") ~ "(" ~ ltlQExpr ~ "," ~ ltlQExpr ~ ")" ^^ {case op~"("~ltl1~","~ltl2~")" => BinOp(op, ltl1, ltl2)} |
    ("ALL" | "EXS") ~ ident ~ ":" ~ dotted ~ "(" ~ ltlQExpr ~ ")" ^^ {case op~v~":"~r~"("~ltl~")" => QuantOp(op, v, r, ltl)} | 
    bool
  
  def bool: Parser[Node] = equality | "TRUE" ^^ {x => Bool(True)} | "FALSE" ^^ {x => Bool(False)} | "(" ~> ltlQExpr <~ ")" | dotted ^^ {Bool(_)}
  def equality: Parser[Bool] = (expr ~ ("==" | "!=" | ">" | "<") ~ expr) ^^ {case l~op~r => Bool(ComparisonOp(op, l, r))}
  def expr: Parser[Dotted] = dotted
  def dotted: Parser[Dotted] = s"$jIdentifier(\\.$jIdentifier)*".r ^^ {str => Dotted(str.split("\\.").toList)}
  
  private val jIdentifier = """\p{javaJavaIdentifierStart}\p{javaJavaIdentifierPart}*"""
  
  private def unsupportedBlock(blockName: String): Parser[NodeOrGarbage] = s"$blockName" ~ "{"~"""[^}]*""".r ~ "}" ^^ {x => None
    throw new CurrentlyUnsupportedSyntaxError(s"The $blockName block")}
  
  private def filterOutTheGarbage(xs: List[NodeOrGarbage]): List[Node] =
    xs filterNot {_ == Garbage} map {_.asInstanceOf[Node]} 
}

class CurrentlyUnsupportedSyntaxError(featureName: String)
extends RuntimeException(s"$featureName is currently unsupported in the property syntax.")

// AST nodes:
sealed abstract class NodeOrGarbage
case object Garbage extends NodeOrGarbage
sealed abstract class Node extends NodeOrGarbage
case class UnOp(op: String, formula: Node) extends Node
case class BinOp(op: String, left: Node, right: Node) extends Node
case class QuantOp(op: String, variable: String, range: Dotted, formula: Node) extends Node
case class Bool(expr: Boolable) extends Node
sealed abstract class Boolable
case class ComparisonOp(op: String, left: Dotted, right: Dotted) extends Boolable
case class Dotted(chain: List[String]) extends Boolable
case object True extends Boolable
case object False extends Boolable
case class Neg(boolExpr: Boolable) extends Boolable