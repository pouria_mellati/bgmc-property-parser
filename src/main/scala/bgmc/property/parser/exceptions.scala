package bgmc.property.parser

class PropertyParsingError(msg: String) extends RuntimeException(msg)
